Source: libcpan-reporter-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Marius Gavrilescu <marius@ieval.ro>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libcapture-tiny-perl <!nocheck>,
                     libconfig-tiny-perl <!nocheck>,
                     libdevel-autoflush-perl <!nocheck>,
                     libfile-homedir-perl <!nocheck>,
                     libfile-pushd-perl <!nocheck>,
                     libmodule-build-perl <!nocheck>,
                     libprobe-perl-perl <!nocheck>,
                     libtest-reporter-perl <!nocheck>,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcpan-reporter-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcpan-reporter-perl.git
Homepage: https://metacpan.org/release/CPAN-Reporter
Rules-Requires-Root: no

Package: libcpan-reporter-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcapture-tiny-perl,
         libconfig-tiny-perl,
         libdevel-autoflush-perl,
         libfile-homedir-perl,
         libprobe-perl-perl,
         libtest-reporter-perl
Description: module which adds CPAN Testers reporting to CPAN.pm
 The CPAN Testers project captures and analyzes detailed results from building
 and testing CPAN distributions on multiple operating systems and multiple
 versions of Perl. This provides valuable feedback to module authors and
 potential users to identify bugs or platform compatibility issues and
 improves the overall quality and value of CPAN.
 .
 One way individuals can contribute is to send a report for each module that
 they test or install. CPAN::Reporter is an add-on for the CPAN.pm module to
 send the results of building and testing modules to the CPAN Testers project.
 Full support for CPAN::Reporter is available in CPAN.pm as of version 1.92.
